from typing import List, Tuple, Sequence

import matplotlib.pyplot as plt


def adjust_yrange(axes: Sequence[plt.Axes]):
    y_ranges: List[Tuple[float, float]] = [ax.get_ylim() for ax in axes]
    y_min: float = min(y_min for y_min, _ in y_ranges)
    y_max: float = max(y_max for _, y_max in y_ranges)
    for ax in axes:
        ax.set_ylim(bottom=y_min, top=y_max)
