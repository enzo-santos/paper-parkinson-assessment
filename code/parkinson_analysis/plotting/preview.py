from typing import Optional, Mapping, Dict, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pywt

from config import UserInfo, TwoDimSequence, DATA_FREQUENCY, FileInfo
from reader import load_users


# noinspection PyPep8Naming
def plot_time(axes: TwoDimSequence[plt.Axes], info: UserInfo, *, title: Optional[str] = None,
              shall_label: bool = True,
              font_settings: Optional[Mapping[str, str]] = None, label_settings: Optional[Mapping[str, str]] = None):
    HAND_SETTINGS = {
        'left': {'label': 'left hand', 'color': 'red', 'alpha': 1.0},
        'right': {'label': 'right hand', 'color': 'green', 'alpha': 0.7}
    }
    SENSOR_SETTINGS = {
        'accelerometer': {'index': 0, 'unit': 'm/s²'},
        'gyroscope': {'index': 1, 'unit': 'rad/s'}
    }

    sensor_ranges: Dict[str, Tuple[float, float]] = {}
    files_info = sorted(info['files'], key=lambda d: d['hand'])
    for file_info in files_info:
        sensor = file_info['sensor']
        data = np.loadtxt(file_info['path'], usecols=(3, 4, 5), skiprows=1, delimiter=',')

        y_min, y_max = sensor_ranges.get(sensor, (float("+inf"), float("-inf")))
        y_min = min(y_min, np.amin(data))
        y_max = max(y_max, np.amax(data))
        sensor_ranges[sensor] = (y_min, y_max)

        file_info['data'] = data

    hand_labels: Dict[str, bool] = {}
    for file_info in files_info:
        sensor = file_info['sensor']
        hand = file_info['hand']
        y = file_info['data']

        j = SENSOR_SETTINGS[sensor]['index']
        color = HAND_SETTINGS[hand]['color']
        alpha = HAND_SETTINGS[hand]['alpha']

        x_min = 0
        x_max = y.shape[0] / DATA_FREQUENCY
        x = np.linspace(x_min, x_max, y.shape[0])

        y_min, y_max = sensor_ranges[sensor]

        for i, y_label in enumerate(('x axis', 'y axis', 'z axis')):
            unit = SENSOR_SETTINGS[sensor]['unit']
            ax: plt.Axes = axes[i][j]

            if title is not None and i == 0:
                ax.set_title(title.format(sensor=sensor), **font_settings)

            if i == 2:
                ax.set_xlabel('Time (s)', **label_settings)
            else:
                ax.set_xticklabels(())

            ax.set_xlim(left=x_min, right=x_max)
            ax.set_ylim(bottom=y_min - .2, top=y_max + .2)
            ax.set_ylabel(f'{y_label} ({unit})', **label_settings)
            ax.xaxis.grid(True)

            is_label = hand_labels.get(hand, False)
            if shall_label and not is_label:
                ax.plot(x, y[:, i], color=color, alpha=alpha, label=HAND_SETTINGS[hand]['label'])
                hand_labels[hand] = True
            else:
                ax.plot(x, y[:, i], color=color, alpha=alpha)


# noinspection PyPep8Naming
def plot_wavelet(ax: plt.Axes, info: FileInfo, *, axis: str = 'x', title: Optional[str] = None,
                 font_settings: Optional[Mapping[str, str]] = None, label_settings: Optional[Mapping[str, str]] = None):
    AXES = {'x': 3, 'y': 4, 'z': 5}
    if axis not in AXES:
        raise ValueError(f"invalid axis: {axis}")

    data = np.loadtxt(info['path'], usecols=(2, AXES[axis]), skiprows=1, delimiter=',')
    x = data[:400, 0]
    y = data[:400, 1]

    if title is not None:
        ax.set_title(title, **font_settings)

    dt = x[1] - x[0]
    coefs, freqs = pywt.cwt(y, np.arange(1, 128), 'cmor', dt)

    period = 1 / freqs
    clevels = np.log2((0.0625, 0.125, 0.25, 0.5, 1, 2, 4, 8))
    qcs = ax.contourf(x, np.log2(period), np.log2(abs(coefs) ** 2), clevels, extend='both', cmap=plt.cm.seismic)
    ax.invert_yaxis()
    ax.set_ylim(ax.get_ylim()[0], -1)
    return qcs


def main():
    info = load_users(r'C:\Users\enzog\Desktop\parkinson_analysis\data')

    fig, axs = plt.subplots(3, 4, figsize=(14, 14))
    plot_time(axs[:, (0, 2)], info[0], title='{sensor}\nhealthy group')
    plot_time(axs[:, (1, 3)], info[-1], title='{sensor}\nparkinson group')
    fig.legend(loc='lower center', prop={'size': 20}, ncol=2)
    fig.subplots_adjust(top=0.9, wspace=0.5)
    plt.show()


if __name__ == '__main__':
    main()
