from typing import Dict, List, Union, TypeVar, Sequence

from numpy import ndarray
from sklearn.base import BaseEstimator
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV, ShuffleSplit
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

T = TypeVar('T')
TwoDimSequence = Sequence[Sequence[T]]
Array = ndarray
Matrix = List[Array]
JsonList = List[Dict[str, str]]

FileInfo = Dict[str, str]
UserInfo = Dict[str, Union[str, List[FileInfo]]]
DatasetInfo = List[UserInfo]

DATA_FREQUENCY: int = 100
"""
Frequency used by the sensors to record the data from the patients.
"""

CLASSIFIERS: List[BaseEstimator] = [
    SVC(gamma="auto", C=1),
    GaussianNB(var_smoothing=1e-8),
    RandomForestClassifier(n_estimators=50),
    GridSearchCV(
        KNeighborsClassifier(),
        {"n_neighbors": range(5, 11)}, cv=5,
    ),
    LogisticRegression(solver="liblinear", penalty="l1", C=1),
    LinearDiscriminantAnalysis(),
    DecisionTreeClassifier(max_depth=6)
]
"""
Classifiers to be used in this analysis.
"""

CV = ShuffleSplit(n_splits=10, test_size=.2, random_state=0)
"""
Cross-validator to be used in this analysis.
"""
