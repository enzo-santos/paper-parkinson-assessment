from typing import Dict

import pandas as pd
from sklearn.ensemble import VotingClassifier
from sklearn.model_selection import ShuffleSplit, cross_val_score

from classification import utils
from config import CLASSIFIERS


def classify(df: pd.DataFrame, *, cv: ShuffleSplit) -> Dict[str, float]:
    """
    Apply a classification in a DataFrame combining different estimators.

    The estimators used for this classification are `SVC`, `GaussianNB`, `RandomForestClassifier`,
    `KNeighborsClassifier`, `LogisticRegression`, `LinearDiscriminantAnalysis` and `DecisionTreeClassifier`. Each
    estimator will receive a sample and attribute it to a class. The most chosen class is returned;

    :param df: the DataFrame containing the dataset. There must be a column named 'outcome' containing the classes
                for each sample (represented by a row of this DataFrame).
    :param cv: the cross-validation to use in the classification.
    :return: a dictionary containing information from this classification. This dictionary contains the
                keys 'mean' and 'std', all mapping to `str`.
    """
    X, y = utils.transform(df)
    classifier = VotingClassifier([(c.__class__.__name__, c) for c in CLASSIFIERS])
    accuracies = cross_val_score(classifier, X, y, cv=cv)
    return {"mean": accuracies.mean(), "stdev": accuracies.std()}
