import pandas as pd
from sklearn.model_selection import GridSearchCV, cross_val_score, ShuffleSplit

from classification import utils
from config import CLASSIFIERS, JsonList


def classify(df: pd.DataFrame, *, cv: ShuffleSplit) -> JsonList:
    """
    Apply a set of classifications in a DataFrame using different estimators.

    The estimators used for these classifications are `SVC`, `GaussianNB`, `RandomForestClassifier`,
    `KNeighborsClassifier`, `LogisticRegression`, `LinearDiscriminantAnalysis` and `DecisionTreeClassifier`.

    :param df: the DataFrame containing the dataset. There must be a column named 'outcome' containing the classes
                for each sample (represented by a row of this DataFrame).
    :param cv: the cross-validation to use in the classifications.
    :return: a list of dictionaries containing information from these classifications. Each dictionary contains the
                keys 'classifiers', 'mean' and 'stdev', mapping to `str`, `float` and `float`, respectively.
    """
    X, y = utils.transform(df)
    json: JsonList = []
    for classifier in CLASSIFIERS:
        classifier.fit(X, y)

        name = classifier.__class__.__name__
        if isinstance(classifier, GridSearchCV):
            name = classifier.best_estimator_.__class__.__name__

        accuracies = cross_val_score(classifier, X, y, cv=cv)
        json.append({
            "classifier": name,
            "mean": accuracies.mean(),
            "stdev": accuracies.std(),
        })

    return json
