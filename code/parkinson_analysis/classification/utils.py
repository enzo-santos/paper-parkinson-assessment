from typing import Tuple

import pandas
from sklearn.preprocessing import StandardScaler

from config import Matrix, Array


def transform(df: pandas.DataFrame, *, scale: bool = True) -> Tuple[Matrix, Array]:
    """
    Transforms a DataFrame into samples and their classes.

    :param df: the DataFrame to be transformed. There must be a column named 'outcome' containing the classes
                for each sample (represented by a row of this DataFrame).
    :param scale: if the returned samples should be normalized into 0.0 and 1.0.
    :return: a tuple containing a matrix of features and an array of classes.
    """
    X = df.drop(columns=["outcome"]).to_numpy()
    if scale:
        X = StandardScaler().fit_transform(X)
    y = df["outcome"].to_numpy()
    return X, y
