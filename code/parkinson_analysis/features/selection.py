import numpy
import pandas as pd
from sklearn.feature_selection import SelectKBest

from classification import utils


def select_percentage(df: pd.DataFrame, percentage: float) -> pd.DataFrame:
    """
    Select the best features from a dataset.

    :param df: the DataFrame containing the dataset. There must be a column named 'outcome' containing the classes
                for each sample (represented by a row of this DataFrame).
    :param percentage: the percentage of the features to be considered. If 1, no selection is done.
    :return: a new DataFrame with shape `(df.shape[0], ceil(df.shape[1] * percentage))` containing the best features
                from the original DataFrame.
    """
    if not 0 < percentage <= 1:
        raise ValueError("percentage must be in (0, 1]")

    if percentage == 1:
        return df

    X, y = utils.transform(df)
    k = int(df.shape[1] * percentage)

    selector = SelectKBest(k=k).fit(X, y)
    indexes = selector.get_support(indices=True)
    cols = numpy.append(df.columns[indexes], "outcome")
    return df.loc[:, cols]
