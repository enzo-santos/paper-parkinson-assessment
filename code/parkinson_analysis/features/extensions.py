from typing import Dict, Iterable, Tuple

import librosa
import numpy as np
import pywt
import scipy.signal
import scipy.stats

from config import DATA_FREQUENCY, Array
from features.extraction import FeatureParser
from utils import split_by_seconds, approximate_entropy, mean_frequency


class _FeatureParserExtension(FeatureParser):
    def __init__(self, parser: FeatureParser):
        self._parser: FeatureParser = parser

    def _generate_features(self, values: Array, outcome: int) -> Dict[str, float]:
        return self._parser._generate_features(values, outcome)

    def _transform(self, values: Array) -> Iterable[Array]:
        yield from self._parser._transform(values)


class ButterworthFilterExtension(_FeatureParserExtension):
    """
    Allows the data filtering on a dataset.
    """
    ORDER: int = 4
    FC: int = 30
    FS: int = DATA_FREQUENCY
    W: float = FC / (FS / 2)

    def _transform(self, values: Array) -> Iterable[Array]:
        """
        Transforms an user array to a group of arrays.

        This method applies a Butterworth filter to the arrays before parsing its features.

        :param values: the array to be transformed.
        :return: the group of transformed arrays.
        """
        values = values - values[0]
        # noinspection PyTupleAssignmentBalance
        b, a = scipy.signal.butter(self.ORDER, self.W)
        values = scipy.signal.filtfilt(b, a, values)
        yield from super()._transform(values)

    def __str__(self):
        return f"<{str(self._parser)[1:-1]}, ButterworthFilter>"


class TimeWindowExtension(_FeatureParserExtension):
    """
    Allows the split into time windows on a dataset.
    """

    def __init__(self, parser: FeatureParser, window_size: int):
        super().__init__(parser)
        self._window_size: int = window_size

    def _transform(self, values: Array) -> Iterable[Array]:
        """
        Transforms an user array to a group of arrays.

        This method splits the arrays into temporal windows.

        :param values: the array to be transformed.
        :return: the group of transformed arrays.
        """

        # Truncates the samples' time length to 2 minutes
        values = values[:120 * DATA_FREQUENCY]
        for wrapper_values in super()._transform(values):
            for value in split_by_seconds(wrapper_values, self._window_size):
                yield value

    def __str__(self):
        return f"<{str(self._parser)[1:-1]}, TimeWindow{self._window_size:02d}Seconds>"


class WaveletTransformExtension(_FeatureParserExtension):
    """
    Allows the analysis of the wavelet domain from a dataset.
    """

    def _generate_features(self, values: Array, outcome: int) -> Dict[str, float]:
        """
        Extracts features from an array.

        The extracted features are the coefficients from linear prediction, entropy, variance and 3rd-order cumulants.

        :param values: the array whose features will be extracted.
        :param outcome: the outcome of the user this array belongs to.
        :return: a dictionary whose keys are the features names mapped to its corresponding values.
        """
        new_features = {}

        # Linear prediction coefficients
        # http://librosa.github.io/librosa/generated/librosa.core.lpc.html
        lp_coefs = librosa.lpc(values, 3)
        for i, coef in enumerate(lp_coefs):
            if i == 0:
                continue

            new_features[f'linear prediction coef {i}'] = coef

        # Wavelet transform detail coefficients
        # https://pywavelets.readthedocs.io/en/latest/ref/dwt-discrete-wavelet-transform.html
        _, c_d = pywt.dwt(values, 'db3')

        # Wavelet transform based entropy and variance
        # http://ataspinar.com/2018/12/21/a-guide-for-using-the-wavelet-transform-in-machine-learning/
        c_d *= 1e+4

        new_features['wvt based entropy'] = approximate_entropy(c_d)
        new_features['wvt based variance'] = np.var(c_d)

        # Higher order cumulants
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.moment.html
        new_features['higher order cumulants'] = scipy.stats.moment(values, moment=3)

        features = self._parser._generate_features(values, outcome)
        features.update(new_features)
        return features

    def __str__(self):
        return f"<{str(self._parser)[1:-1]}, WaveletTransform>"


class PowerRatioExtension(_FeatureParserExtension):
    """
    Allows the analysis of the power ratio from a dataset.
    """

    def _generate_features(self, values: Array, outcome: int) -> Dict[str, float]:
        """
        Extracts features from an array.

        The extracted feature is the power ratio from the frequencies 1-6Hz and 6-12Hz.

        :param values: the array whose features will be extracted.
        :param outcome: the outcome of the user this array belongs to.
        :return: a dictionary whose keys are the features names mapped to its corresponding values.
        """
        new_features = {}

        xf = np.linspace(0, DATA_FREQUENCY / 2, values.shape[0] // 2 + 1)
        yf = np.absolute(np.fft.rfft(values))
        assert xf.size == yf.size

        num = yf[(xf >= 1) & (xf <= 6)]
        den = yf[(xf >= 6) & (xf <= 12)]
        new_features['power ratio'] = num.mean() / den.mean()

        features = self._parser._generate_features(values, outcome)
        features.update(new_features)
        return features


class FrequencyDomainExtension(TimeWindowExtension):
    """
    Allows the analysis of the frequency domain from a dataset.
    """

    def __init__(self, parser: TimeWindowExtension):
        super().__init__(parser, parser._window_size)

    def _generate_features(self, values: Tuple[Array, Array], outcome: int) -> Dict[str, float]:
        """
        Extracts features from an array.

        The extracted features are peak value, peak frequency, mean frequency, skewness and kurtosis.

        :param values: the array whose features will be extracted. This array should represent a frequency domain.
        :param outcome: the outcome of the user this array belongs to.
        :return: a dictionary whose keys are the features names mapped to its corresponding values.
        """
        y, yf = values
        features = self._parser._generate_features(y, outcome)
        features.update({
            'peak value (FFT)': yf.max(),
            'peak frequency (FFT)': yf[np.argmax(yf)],
            'mean frequency (FFT)': mean_frequency(yf, self._window_size),
            'skew (FFT)': scipy.stats.skew(yf),
            'kurtosis (FFT)': scipy.stats.kurtosis(yf),
        })
        return features

    def _transform(self, values: Array) -> Iterable[Tuple[Array, Array]]:
        """
        Transforms an user array to a group of arrays.

        For each array transformed, it's calculated its frequency domain using a Fast Fourier Transform algorithm.

        :param values: the array to be transformed.
        :return: the group of transformed arrays.
        """
        for y in self._parser._transform(values):
            xf = np.linspace(0, DATA_FREQUENCY / 2, DATA_FREQUENCY * self._window_size // 2 + 1)
            yf = np.absolute(np.fft.rfft(y))
            yield y, yf[xf >= 1] / yf.size

    def __str__(self):
        return f"<{str(self._parser)[1:-1]}, FrequencyDomain>"
