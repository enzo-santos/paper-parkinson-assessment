import abc
from typing import Dict, Iterable, List

import numpy as np
import pandas as pd
import scipy.stats

from config import Array, DatasetInfo


class FeatureParser(abc.ABC):
    """
    Provides the extraction of features from a dataset.
    """

    def parse(self, info: DatasetInfo) -> pd.DataFrame:
        """
        Parses features from a dataset.

        :param info: the information of this dataset.
        :return: a DataFrame containing the features from this dataset.
        """
        df = pd.DataFrame()

        for user_info in info:
            outcome = 1 if user_info["outcome"] == "parkinson" else 0

            user_samples: List[Dict[str, float]] = []
            for file_info in user_info["files"]:
                columns = np.loadtxt(file_info["path"], usecols=(3, 4, 5), skiprows=1)
                columns = np.c_[columns, np.linalg.norm(columns, axis=1)]

                for i, axis_label in enumerate(('x', 'y', 'z', 'norm')):
                    column = columns[:, i]

                    for j, values in enumerate(self._transform(column)):
                        sample: Dict[str, float]
                        if j in range(len(user_samples)):
                            sample = user_samples[j]
                        else:
                            sample = {"outcome": outcome}
                            user_samples.append(sample)

                        features = self._generate_features(values, outcome)
                        sample.update({
                            f"{file_info['sensor']} {file_info['hand']} {axis_label} {feature}": value
                            for feature, value in features.items()
                        })

            for sample in user_samples:
                df = df.append(pd.Series(sample), ignore_index=True, sort=True)

        return df

    @abc.abstractmethod
    def _generate_features(self, values: Array, outcome: int) -> Dict[str, float]:
        """
        Extracts features from an array.

        :param values: the array whose features will be extracted.
        :param outcome: the outcome of the user this array belongs to.
        :return: a dictionary whose keys are the features names mapped to its corresponding values.
        """
        pass

    def _transform(self, values: Array) -> Iterable[Array]:
        """
        Transforms an user array to a group of arrays.

        This method can be extends to support splitting the arrays into temporal windows. By default, this method
        returns a iterable with a single element being the array passed as input.

        :param values: the array to be transformed.
        :return: the group of transformed arrays.
        """
        yield values


class TimeDomainFeatureParser(FeatureParser):
    """
    Parses time domain features from the users.

    The parsed features are range, standard deviation, skewness, kurtosis and Root Mean Square.
    """

    def _generate_features(self, values: Array, _) -> Dict[str, float]:
        # noinspection PyArgumentList
        return {
            "range": values.max() - values.min(),
            "stdev": values.std(),
            "skew": scipy.stats.skew(values),
            "kurtosis": scipy.stats.kurtosis(values),
            "rms": np.sqrt(np.mean(values ** 2)),
        }

    def __str__(self):
        return "<TimeDomainReader>"
