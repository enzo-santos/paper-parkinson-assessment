import concurrent.futures
from typing import Callable, TypeVar, List, Optional

import numpy as np

from config import DATA_FREQUENCY, Array, Matrix

T = TypeVar('T')
R = TypeVar('R')


def map_parallel(function: Callable[[T], R], iterable: List[T], *, n_threads: int = 4) -> List[R]:
    """
    A parallel equivalent of the map() built-in function. It blocks until the result is ready.

    This method chops the iterable into a number of chunks which it submits to the process pool as separate tasks.

    :param function: the function to apply.
    :param iterable: the iterable to transform.
    :param n_threads: the number of threads to use. If `n_threads == len(iterable)`, there will be a thread transforming
                        each element from the iterable at the same time.
    :return: the mapping applied to this iterable.
    """
    pool = concurrent.futures.ThreadPoolExecutor(n_threads)
    futures = [pool.submit(function, arg) for arg in iterable]
    return [future.result() for future in futures]


def split_by_seconds(array: Array, seconds: int, frequency: int = DATA_FREQUENCY) -> Matrix:
    """
    Splits an array in temporal windows.

    The data is considered to be added sequentially in some frequency.

    If `array.size % (seconds * frequency) != 0`, it will be created an additional
    array with the last `array.size // (seconds * frequency)` elements.

    >>> y = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
    >>> result = split_by_seconds(y, seconds=3, frequency=1)
    >>> # frequency=1, each value in the array corresponds to 1 second
    >>> assert result == [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
    >>> result = split_by_seconds(y, seconds=3, frequency=2)
    >>> # frequency=2, each two values in the array corresponds to 1 second
    >>> assert result == [[0, 1, 2, 3, 4, 5], [6, 7, 8, 9, 10, 11]]

    :param array: the array containing the data to be split.
    :param seconds: the time window of the split.
    :param frequency: the frequency the data was recorded, in Hz.
    :return: a group of sub-arrays.
    """
    array = np.array(array)
    seconds *= frequency
    n, r = divmod(array.size, seconds)

    if r == 0:
        return np.split(array, n)
    return np.split(array[:n * seconds], n) + [array[-seconds:]]


def mean_frequency(yf: Array, n_sec: Optional[int] = None) -> float:
    """
    Calculates the mean frequency of an array.

    Mean frequency is a pitch measure that assesses the center of the distribution of power across frequencies. Because
    of that, the array should represent a frequency domain.

    :param yf: the array.
    :param n_sec: the number of seconds in the time domain of this array. If `None`, the number of seconds is considered
                    to be the size of the array divided by the project data frequency (i.e. 100 Hz).
    :return: the mean frequency of this array.
    """
    n_sec = yf.size // DATA_FREQUENCY if n_sec is None else n_sec
    xf = np.linspace(0, DATA_FREQUENCY / 2, DATA_FREQUENCY * n_sec // 2 + 1)
    xf = xf[xf >= 1]

    total_area = np.trapz(yf, xf)
    for i, x in enumerate(xf):
        partial_area = np.trapz(yf[:i], xf[:i])
        if partial_area > total_area / 2:
            return xf[i - 1]


def approximate_entropy(a: Array, m: int = 2, r: int = 3) -> float:
    """
    Calculates the approximate entropy from an array.

    Approximate entropy is a technique used to quantify the amount of regularity and the unpredictability of
    fluctuations over time-series data. Because of that, the array should represent a time domain.

    :param a: the array.
    :param m: the length of compared run of data.
    :param r: the filtered level to be used.
    :return: the approximate entropy from this array.
    """
    a = np.array(a)
    n = a.shape[0]

    def _phi(m_):
        z = n - m_ + 1.0
        x = np.array([a[i:i + m_] for i in range(int(z))])
        x_ = np.repeat(x[:, np.newaxis], 1, axis=2)
        c = np.sum(np.absolute(x - x_).max(axis=2) <= r, axis=0) / z
        return np.log(c).sum() / z

    return abs(_phi(m + 1) - _phi(m))
