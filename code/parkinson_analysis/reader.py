import glob
import os
import re
from typing import Optional, Dict

from config import DatasetInfo, UserInfo
from features.extensions import ButterworthFilterExtension, WaveletTransformExtension, PowerRatioExtension, \
    TimeWindowExtension, FrequencyDomainExtension
from features.extraction import FeatureParser, TimeDomainFeatureParser

HEALTHY_FILE_PATTERN = re.compile(r"^controle-(\w+)-(\w+?)_.*_(Accelerometer|Gyroscope)")
"""
A regular expression pattern to detect files containing records of healthy users.

The first group expands to the user id, the second group expands to the hand used in the record (which can assume
the values of 'esquerda' for the left hand and 'direita' for the right hand) and the third group expands to the sensor
used in the record.
"""

PARKINSON_FILE_PATTERN = re.compile(r"^(\w+)(ESQUERDA|DIREITA)_.*[_-](Accelerometer|Gyroscope)")
"""
A regular expression pattern to detect files containing records of users with Parkinson disease.

The first group expands to the user id, the second group expands to the hand used in the record (which can assume
the values of 'esquerda' for the left hand and 'direita' for the right hand) and the third group expands to the sensor
used in the record.
"""


def load_users(path: str) -> DatasetInfo:
    """
    Load users information from a dataset.

    :param path: the root directory of the dataset to be read. There must be two folders within this directory, namely
                    'healthy' and 'parkinson'. Inside these folders, there must be CSV files, which contains the records
                    made with each user. For a file being considered in this analysis, its filename must match a regular
                    expression depending on its user health status. If the user is healthy, its record filename must
                    match `HEALTHY_FILE_PATTERN`, otherwise if the user contains the Parkinson disease, its record
                    filename must match `PARKINSON_FILE_PATTERN`. Additionally, the filename must have a .csv extension.
    :return: a list of dictionaries containing information from each user in the dataset. Each dictionary contains the
                keys 'id', 'outcome', and 'files', mapping to `str`, `str` and `Dict[str, str]`, respectively. Each
                'files' dictionary contains the keys `sensor`, `hand` and `path`, all mapping to `str`.
    """
    json: Dict[str, UserInfo] = {}
    for outcome_folder in os.listdir(path):
        pattern = PARKINSON_FILE_PATTERN if outcome_folder == 'parkinson' else HEALTHY_FILE_PATTERN

        pathname = os.path.join(path, outcome_folder, "*.csv")
        for file_path in glob.glob(pathname):
            _, filename = os.path.split(file_path)

            match = pattern.search(filename)
            if match:
                user_id = match.group(1).lower()
                hand = match.group(2).lower()
                sensor = match.group(3).lower()

                user_json: UserInfo
                user_json = json.setdefault(user_id, {"id": user_id, "outcome": outcome_folder, "files": []})
                user_json["files"].append({
                    "sensor": sensor,
                    "hand": "left" if hand == "esquerda" else "right",
                    "path": file_path,
                })

    return list(json.values())


def get_parser(*, time_window: Optional[int] = None, add_butterworth: bool = True, add_fft: bool = True,
               add_power_ratio: bool = True, add_wavelet: bool = True) -> FeatureParser:
    """
    Returns a customized `FeatureParser`.

    :param time_window: the time window the data must be split before parsing (if `None`, the data will not be split).
    :param add_butterworth: if the Butterworth filter must be applied to the data before parsing.
    :param add_fft: if the frequency domain analysis obtained by a Fast Fourier Transform must be added to the parsing.
    :param add_power_ratio: if the power ratio analysis must be added to the parsing.
    :param add_wavelet: if the wavelet domain analysis obtained by a Wavelet Transform must be added to the parsing.
    :return: a `FeatureParser` with the provided settings.
    """
    parser: FeatureParser = TimeDomainFeatureParser()
    if add_butterworth:
        parser = ButterworthFilterExtension(parser)
    if add_wavelet:
        parser = WaveletTransformExtension(parser)
    if add_power_ratio:
        parser = PowerRatioExtension(parser)
    if time_window is None:
        return parser
    parser = TimeWindowExtension(parser, time_window)
    if add_fft:
        # noinspection PyTypeChecker
        parser = FrequencyDomainExtension(parser)
    return parser
