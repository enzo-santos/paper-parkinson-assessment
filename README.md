# Hand Resting Tremor Assessment of Healthy and Patients With Parkinson’s Disease: An Exploratory Machine Learning Study

by
Ana Camila Alves de Araújo,
Enzo Gabriel da Rocha Santos,
Karina Santos Guedes de Sá,
Viviane Kharine Teixeira Furtado,
Felipe Augusto Santos,
Ramon Costa de Lima,
Lane Viana Krejcová,
Bruno Lopes Santos Lobato,
Gustavo Henrique Lima Pinto,
André dos Santos Cabral,
Anderson Belgamo,
Bianca Callegari,
Ana Francisca Rozin Kleiner,
Anselmo de Athayde Costa e Silva,
Givago da Silva Souza

This paper [has been published](https://doi.org/10.3389/fbioe.2020.00778) in *Frontiers in Bioengineering and Biotechnology*.


## Abstract

The aim of this study is comparing the accuracies of machine learning
algorithms to classify data concerning healthy subjects and patients with
Parkinson’s Disease (PD), toward different time window lengths and a number
of features. Thirty-two healthy subjects and eighteen patients with PD took
part on this study. The study obtained inertial recordings by using an
accelerometer and a gyroscope assessing both hands of the subjects during
hand resting state. We extracted time and temporal frequency domain
features to feed seven machine learning algorithms: k-nearest-neighbors
(kNN); logistic regression; support vector classifier (SVC); linear
discriminant analysis; random forest; decision tree; and gaussian Naïve
Bayes. The accuracy of the classifiers was compared using different numbers
of extracted features (i.e., 272, 190, 136, 82, and 27) from different time
window lengths (i.e., 1, 5, 10, and 15 s). The inertial recordings were
characterized by oscillatory waveforms that, especially in patients with
PD, peaked in a frequency range between 3 and 8 Hz. Outcomes showed that
the most important features were the mean frequency, linear prediction
coefficients, power ratio, power density skew, and kurtosis. We observed
that accuracies calculated in the testing phase were higher than in the
training phase. Comparing the testing accuracies, we found significant
interactions among time window length and the type of classifier (p <
0.05). The study found significant effects on estimated accuracies,
according to their type of algorithm, time window length, and their
interaction. kNN presented the highest accuracy, while SVC showed the worst
results. kNN feeding by features extracted from 1 and 5 s were the
combination with more frequently highest accuracies. Classification using
few features led to similar decision of the algorithms. Moreover,
performance increased significantly according to the number of features
used, reaching a plateau around 136. Finally, the results of this study
suggested that kNN was the best algorithm to classify hand resting tremor
in patients with PD.


## Software implementation

All source code used to generate the results and figures in the paper are in
the `code` folder.
The calculations and figure generation are all run inside
[Jupyter notebooks](http://jupyter.org/).
The data used in this study is provided in `data`.
See the `README.md` files in each directory for a full description.


## Getting the code

You can download a copy of all the files in this repository by cloning the
[git](https://git-scm.com/) repository:

    git clone https://gitlab.com/enzo-santos/paper-parkinson-assessment.git


## Dependencies

You'll need a working Python environment to run the code.
The recommended way to set up your environment is through the
[Anaconda Python distribution](https://www.anaconda.com/download/) which
provides the `conda` package manager.
Anaconda can be installed in your user directory and does not interfere with
the system Python installation.
The required dependencies are specified in the file `environment.yml`.

We use `conda` virtual environments to manage the project dependencies in
isolation.
Thus, you can install our dependencies without causing conflicts with your
setup (even with different Python versions).

Run the following command in the repository folder (where `environment.yml`
is located) to create a separate environment and install all required
dependencies in it:

    conda env create -f environment.yml


## Reproducing the results

Before running any code you must activate the conda environment:

    source activate parkinson_analysis

or, if you're on Windows:

    conda activate parkinson_analysis

This will enable the environment for your current terminal session.
Any subsequent commands will use software that is installed in the environment.

To explore the code results, you can execute the Jupyter notebooks
individually.
To do this, you must first start the notebook server by going into the
repository top level and running:

    jupyter notebook

This will start the server and open your default web browser to the Jupyter
interface. In the page, go into the `code/notebooks` folder and select the
notebook that you wish to view/run.

The notebook is divided into cells (some have text while other have code).
Each cell can be executed using `Shift + Enter`.
Executing text cells does nothing and executing code cells runs the code
and produces it's output.
To execute the whole notebook, run all cells in order.
